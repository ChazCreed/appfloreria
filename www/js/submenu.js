$(document).ready(function () {
    
    var _url = window.location.search;  
    
    GetStoreData(getUrlParams(_url).id); 
});

// Initialize collapse button
$(".button-collapse").sideNav(); 
//Sidebar Size
$('.button-collapse').sideNav({
    menuWidth: 250, // Default is 300
    edge: 'left', // Choose the horizontal origin
    closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
    draggable: true // Choose whether you can drag to open on touch screens
});
 
function GetStoreData(StoreId)
{  
    $.ajax({
        type: 'GET',
        url: 'https://appfloreria.herokuapp.com/api/v1/store/'+ StoreId.toString(),            
        headers: {
            'Content-Type': 'application/json',
        },
        dataType: "JSON", 
        async: false, 
        success: function(data){ 
            console.log(data.store) 
        },            
        error: function (errMsg) {
            console.log(errMsg)
            if(errMsg.responseJSON){
                Materialize.toast(errMsg.responseJSON.message, 10000)
            }
            else
            {        
                Materialize.toast('Error al connectar con el servidor, Por favor <br> intente nuevamente', 10000)
            }
        }
    });
}

function getUrlParams(url)
{ 
  var urlParams = {};
  url.replace(
    new RegExp("([^?=&]+)(=([^&]*))?", "g"),
    function($0, $1, $2, $3) {
      urlParams[$1] = $3;
    }
  );
  
  return urlParams;
} 
