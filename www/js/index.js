num = 1;
i = 1;
storesArray = null;
showMore = 0;
setInterval(function () {
    $("#cf").fadeOut(1000, function () {
        num++;
        if (num > 3)
            num = 1;
        $("#cf").attr("src", "img/" + num + ".jpg");
    }).fadeIn(1000);
    $("#cf2").fadeOut(1000, function () {
        num++;
        if (num > 3)
            num = 1;
        $("#cf2").attr("src", "img/" + num + ".jpg");
    }).fadeIn(1000);
}, 5000);

$(document).ready(function () {
    if(!localStorage.getItem("User"))
    {
        window.location.href = './login.html';
    }
    $('#loader').fadeOut(500, function () { $('#loader').remove(); }); 
    $('.carousel').carousel();

    GetStores(); 
    $('#logout').click(function () 
    { 
        localStorage.clear();
        window.location.href = './login.html';
    });
});

// Initialize collapse button
$(".button-collapse").sideNav(); 
//Sidebar Size
$('.button-collapse').sideNav({
    menuWidth: 250, // Default is 300
    edge: 'left', // Choose the horizontal origin
    closeOnClick: true, // Closes side-nav on <a> clicks, useful for Angular/Meteor
    draggable: true // Choose whether you can drag to open on touch screens
});

function GetStores(){  
    $.ajax({
        type: 'GET',
        url: 'https://appfloreria.herokuapp.com/api/v1/stores/',            
        headers: {
            'Content-Type': 'application/json',
        },
        dataType: "JSON", 
        async: false, 
        success: function(data){
            storesArray = data.stores;
            console.log(storesArray)
            LoadStoreCards()
        },            
        error: function (errMsg) {
            console.log(errMsg)
            if(errMsg.responseJSON){
                Materialize.toast(errMsg.responseJSON.message, 10000)
            }
            else
            {        
                Materialize.toast('Error al connectar con el servidor, Por favor <br> intente nuevamente', 10000)
            }
        }
    });
}

function LoadStoreCards(){ 
    showMore = i + 5 
    if(storesArray.length <= 0)
    {
        $('#startFast').one('click',function() {  
            window.location.href = 'index.html';
        });
    }
    else
    {
        for(var x = 0; x < storesArray.length; x++)
            {
                $("#StoreCards").append(
                '<center>'+
                    '<div id="card">'+
                        '<a href="submenu.html?id='+ storesArray[i]._id +' ">'+
                            '<div class="card" style="position:relative; z-index:100">'+
                                '<div class="pics_card" style="background-image: url(https://i.ytimg.com/vi/_sPIYFOcdLc/maxresdefault.jpg);">'+
                                    '<div class="txt_titre_card"></div>'+
                                '</div>'+
                                '<div class="txt_desciption_card"><h3>'+ storesArray[i].store_name +'</h3>'+ storesArray[i].store_description +'</div>'+

                                '<div align="left" style="position:absolute; width:100%;background-color: #ffffff;">'+
                                    '<hr style="color:#bfbfbf">'+
                                    '<center>'+
                                        '<div class="tool_link" style="color:#ff8f00; margin-top:1%"><i class="fa fa-star fa-2x" aria-hidden="true"></i></div>'+
                                        '<div class="tool_link" style="color:#ff8f00; margin-top:1%; margin-left: -3%"><i class="fa fa-star fa-2x" aria-hidden="true"></i></div>'+
                                        '<div class="tool_link" style="color:#ff8f00; margin-top:1%; margin-left: -3%"><i class="fa fa-star fa-2x" aria-hidden="true"></i></div>'+
                                        '<div class="tool_link" style="color:#ff8f00; margin-top:1%; margin-left: -3%"><i class="fa fa-star fa-2x" aria-hidden="true"></i></div>'+
                                        '<div class="tool_link" style="color:#ff8f00; margin-top:1%; margin-left: -3%"><i class="fa fa-star fa-2x" aria-hidden="true"></i></div>'+
                                    '</center>'+
                                '</div>'+
                            '</div>'+
                        '</a>'+
                    '</div>'+
                    '<br>'+
                '</center>'
                );
            }
        i = showMore;
    }
}
