$( document ).ready(function() {
    console.log( "ready!" );

});


$(document).on('click','#login-btn',function(){

    if(!$('#username').val())
    { 
        if( !$('#password').val())
        { 
            Materialize.toast('Por favor Ingrese un Usuario y Contrasena', 10000)
        }else
        {
            Materialize.toast('Por favor Ingrese Su nombre de Usuario', 10000)
        }
    }
    else if( !$('#password').val())
    { 
        Materialize.toast('Por favor Ingrese su Contrasena', 10000)
    } 
    else
    {
        $.ajax({
            type: 'POST',
            url: 'https://appfloreria.herokuapp.com/api/v1/user/login',            
            headers: {
                'Content-Type': 'application/json',
            },
            dataType: "JSON", 
            async: false,data: JSON.stringify( {
                "user_name":$('#username').val(),
                "password": $('#password').val(), 
            }), 
            success: function(data){
                console.log(data)
                var logged_user = {
                        "_id":data.user[0]._id,
                        "user_name":data.user[0].user_name,
                        "password":data.user[0].password,
                        "first_name":data.user[0].first_name,
                        "last_name":data.user[0].last_name,
                        "email":data.user[0].email,
                        "address":data.user[0].address,
                        "phone":data.user[0].phone
                    }  
                localStorage.setItem("User", JSON.stringify(logged_user)) 
                window.location.href = './index.html';
            },            
            error: function (errMsg) {
                console.log(errMsg)
                if(errMsg.responseJSON){
                    Materialize.toast(errMsg.responseJSON.message, 10000)
                }
                else
                {        
                    Materialize.toast('Error al connectar con el servidor, Por favor <br> intente nuevamente', 10000)
                }
            }
        }); 
    }

});
